emacs-ivy (0.14.2-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 19:28:25 +0900

emacs-ivy (0.14.2-1) unstable; urgency=medium

  * New upstream release.

 -- Nicholas D Steeves <sten@debian.org>  Sun, 29 Oct 2023 18:52:35 -0400

emacs-ivy (0.14.0-1) unstable; urgency=medium

  * New upstream release.
  * Depend on elpa-wgrep (>= 3.0.0).
  * Update upstream copyright years.
  * Declare Standards-Version 4.6.2 (no changes required).

 -- Nicholas D Steeves <sten@debian.org>  Mon, 12 Jun 2023 21:32:35 -0400

emacs-ivy (0.13.4+78.gb8be491-1) unstable; urgency=medium

  * New upstream snapshot (Closes: #1020180).
  * Debian 9 (stretch, oldoldstable) has Emacs (>= 46.0), so it is no longer
    necessary to qualify Recommends.
  * Declare Standards-Version 4.6.1 (no changes required).

 -- Nicholas D Steeves <sten@debian.org>  Sun, 27 Nov 2022 15:57:40 -0500

emacs-ivy (0.13.4-1) unstable; urgency=medium

  * Upload to unstable.
  * Note "Interactive and searchable yank-pop/kill-ring/clipboard history
    interface" in long description, and also note that Ivy is an alternative
    to Ido.
  * Update my copyright years.
  * Migrate to debhelper-compat 13.
    + Cleanup tmp/changelog to work around a bug in debhelper, which errors
      because it thinks the changelog has not been installed to a bin:pkg,
      even though it has been, explicitly, with dh_installchangelogs.
  * Declare Standards-Version 4.6.0 (no changes required).
  * Drop dh_clean override, because it is no longer needed.

 -- Nicholas D Steeves <sten@debian.org>  Sat, 26 Feb 2022 08:42:28 -0500

emacs-ivy (0.13.4-1~exp1) experimental; urgency=low

  * New upstream release.
  * Rebase quilt series onto this release:
    - Drop 0001-ivy-test.el-ivy-with-Don-t-use-counsel-locate-git-ro.patch
  * Add ivy-faces.el to elpa-ivy.elpa; it is a hard dependency of ivy.el and
    is bundled with Ivy on both MELPA and ELPA.
  * Update upstream copyright years.
  * Update my email address and copyright years.
  * Drop unnecessary README.source, which hasn't been relevant since
    0.13.0-1; it does not add anything to the long description nor the
    upstream README and is not needed for dfsg compliance.
  * Add elpa-smex to Enhances.
  * Add 0002-Skip-ivy-read-directory-name-and-rely-on-manual-test.patch

 -- Nicholas D Steeves <sten@debian.org>  Sat, 27 Mar 2021 22:37:34 -0400

emacs-ivy (0.13.0-1) unstable; urgency=medium

  * New upstream version, now with DFSG-free documentation.
  * Drop 0001-Skip-3-tests-that-are-unsuitable-for-buildd-or-DebCI.patch
    because these tests no longer exist upstream.
  * copyright: No longer exclude doc/ivy.org and doc/ivy.texi.
  * copyright: Add GDFL-1.3+NIV license info for these two files.
  * control: Add texinfo as a build dep when 'nodoc' isn't active.
  * rules: Add override_dh_installinfo to convert upstream ivy.org to
    ivy.texi, and to then run makeinfo.
  * rules: Override dh_clean to restore upstream-provided ivy.texi
  * Create debian/elpa-ivy.info to install the new info page.
  * Add elpa-smex to elpa-counsel's Recommends.  The way it presents one's
    recently and most frequently used commands at the top of the
    candidates list is a wonderful productivity-enhancing feature.  See
    the docs to learn how to use Counsel to enable this for the M-x
    interface.
  * Add elpa-wgrep as a build-dep. (Closes: #944991)
  * Cherry pick 0001-ivy-test.el-ivy-with-Don-t-use-counsel-locate-git-ro.patch
    from upstream to fix failing self-tests.
  * Add 0002-Skip-ert-deftest-ivy-lazy-load-ffap-ffap-url-p.patch to skip
    another failing test.
  * debian/control: Add "Rules-Requires-Root: no".
  * Declare Standards-Version 4.5.0 (no changes required).

 -- Nicholas D Steeves <nsteeves@gmail.com>  Fri, 24 Jul 2020 19:12:20 -0400

emacs-ivy (0.12.0+dfsg-1) unstable; urgency=medium

  * New upstream version.
  * Switch to debhelper-compat 12.
  * Fix typos in long descriptions.
  * Reflect upstream's updated copyright years in d/copyright.
  * Update counsel's description with the most noteworthy new functionality.
  * Add 0001-Skip-3-tests-that-are-unsuitable-for-buildd-or-DebCI.patch
    Tests depend on network access.
  * override_dh_installchangelogs: Install Changelog.org as changelog, and
    append local-var-snippet to activate org-mode when visiting this file
    using Emacs.
  * gbp.conf: Disable pristine-tar, which is no longer used in this package.
  * Drop debian/TODO.
  * Add Upstream-Contact.
  * Make it explicit that we're not using the upstream Makefile by echoing
    this in override_dh_auto_build.
  * Declare Standards-Version 4.4.0 (no changes required).

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sun, 25 Aug 2019 19:26:25 -0400

emacs-ivy (0.11.0+dfsg-1) unstable; urgency=medium

  * Switch Vcs from alioth to salsa.
  * Switch to debhelper and compat level 12.
  * Change section to editors, which is more accurate and discoverable.
  * Update Maintainer team name and email address.
  * Drop unused build dependency on pandoc. (unused since 0.10.0+dfsg-1)
    - ivy.el uses org-mode to view ivy-help.org in 'describe-mode.
  * Add Suggests: elpa-flx, which adds fuzzy matching support to Ivy.
  * Add Suggests: elpa-avy, which adds balanced decision tree-based
    traversal of selection candidates.
  * control: Enhance descriptions with better grammar and clarity.
  * control: Update Enhances section for elpa-counsel.
  * Update my copyright years.
  * debian/watch: Repack the +dfsg orig tar using xz compression.
  * Declare Standards-Version 4.3.0. (No additional changes needed)
  * control: Add Enhances elpa-hydra.
  * rules: Drop override_dh_installchangelogs, and use
    debian/elpa-ivy.docs to install the upstream Changelog.org to
    /usr/share/doc/elpa-ivy as-is and in .org format.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Mon, 28 Jan 2019 01:17:02 -0700

emacs-ivy (0.10.0+dfsg-1) unstable; urgency=medium

  * New upstream version 0.10.0+dfsg
  * Declare Standards-Version: 4.1.2. (No additional changes needed)
  * Drop 0001-Prevent-void-function-colir-color-parse-in-ivy-test.patch
    - It has been merged upstream.

 -- Nicholas D Steeves <nsteeves@gmail.com>  Sun, 17 Dec 2017 00:14:08 -0500

emacs-ivy (0.9.1+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #863216)
  * Add 0001-Prevent-void-function-colir-color-parse-in-ivy-test.patch
    - Needed for self-tests to succeed
  * debian/copyright: Exclude non-free GFDL docs (doc/ivy.org and
    doc/ivy.texi)

 -- Nicholas D Steeves <nsteeves@gmail.com>  Fri, 23 Jun 2017 13:41:41 -0400
